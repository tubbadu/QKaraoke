function generateRoundRobinSchedule(players) {
    let playersWork = [...players];
    let n = playersWork.length;
    if (n % 2 === 1) playersWork.push("BYE"), n++;
    
    let rounds = n - 1;
    let schedule = [];
    let playerList = [...playersWork];
    
    for (let r = 0; r < rounds; r++) {
        let matches = [];
        for (let i = 0; i < n / 2; i++) {
            if (playerList[i] !== "BYE" && playerList[n - 1 - i] !== "BYE") {
                matches.push([playerList[i], playerList[n - 1 - i]]);
            }
        }
        schedule.push(...matches);
        playerList = [playerList[0], playerList[n - 1], ...playerList.slice(1, n - 1)];
    }
    return schedule;
}

function computeWaitingTimes(schedule, players) {
    let totalRounds = schedule.length;
    let appearances = {};
    players.forEach(p => appearances[p] = []);
    
    schedule.forEach((match, index) => {
        match.forEach(p => appearances[p].push(index));
    });
    
    let waiting = {};
    players.forEach(p => {
        let idxs = appearances[p];
        let gaps = [idxs[0] + 1];
        for (let i = 0; i < idxs.length - 1; i++) {
            gaps.push(idxs[i + 1] - idxs[i] - 1);
        }
        gaps.push(totalRounds - idxs[idxs.length - 1]);
        waiting[p] = {
            min: Math.min(...gaps),
            max: Math.max(...gaps),
            avg: gaps.reduce((a, b) => a + b, 0) / gaps.length
        };
    });
    return waiting;
}

function validSwap(schedule, i, j) {
    function checkPos(pos) {
        let current = schedule[pos];
        if (pos > 0 && schedule[pos - 1].some(x => current.includes(x))) return false;
        if (pos < schedule.length - 1 && schedule[pos + 1].some(x => current.includes(x))) return false;
        return true;
    }
    [schedule[i], schedule[j]] = [schedule[j], schedule[i]];
    let isValid = checkPos(i) && checkPos(j);
    [schedule[i], schedule[j]] = [schedule[j], schedule[i]];
    return isValid;
}

function optimizeScheduleAdjacent(players, maxIter = 500) {
    let schedule = generateRoundRobinSchedule(players);
    let realPlayers = players.filter(p => p !== "BYE");
    let waiting = computeWaitingTimes(schedule, realPlayers);
    let currentMax = Math.max(...Object.values(waiting).map(w => w.max));
    
    let improved = true, iterCount = 0;
    while (improved && iterCount < maxIter) {
        improved = false;
        iterCount++;
        
        for (let i = 0; i < schedule.length - 1; i++) {
            let j = i + 1;
            if (!validSwap(schedule, i, j)) continue;
            
            [schedule[i], schedule[j]] = [schedule[j], schedule[i]];
            let newWaiting = computeWaitingTimes(schedule, realPlayers);
            let newMax = Math.max(...Object.values(newWaiting).map(w => w.max));
            
            if (newMax < currentMax) {
                waiting = newWaiting;
                currentMax = newMax;
                improved = true;
                break;
            } else {
                [schedule[i], schedule[j]] = [schedule[j], schedule[i]];
            }
        }
    }
    return { schedule, waiting };
}

// // Example Usage
// let players = ["1", "2", "3", "4", "5", "6", "7", "8"];
// let { schedule, waiting } = optimizeScheduleAdjacent(players);
// 
// console.log("Optimized Schedule:");
// schedule.forEach(match => console.log(match));
// 
// console.log("\nWaiting Times Analysis:");
// players.forEach(p => {
//     if (waiting[p]) {
//         let { min, max, avg } = waiting[p];
//         console.log(`Player ${p}: min = ${min}, max = ${max}, avg = ${avg.toFixed(2)}`);
//     }
// });
