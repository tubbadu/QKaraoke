/*const fs = require('fs');

// Read data from JSON file
let data = JSON.parse(fs.readFileSync('data.json', 'utf-8'));
*/
// let failCount = 0;
// let turnsCount = {};
// let lastPlayers = [null, null];
let data;
let availablePlayers = [];
let musicFolderPath = "";
let players = [];
let activePlayers = [];
let sindex = 0
let schedule;

function init(d){
	data = d;
	fetchPlayers(data);
	// console.warn(players)
	// schedule = PlayerScheduler.optimizeScheduleAdjacent(players).schedule;
	// console.warn(schedule)
}


function extractNext() {
	if(!data) {
		console.warn("Warning: data has not been initialized yet."); 
		return;
	}

	const [player1, player2] = schedule[sindex];
	sindex++;
	if(sindex >= schedule.length){
		sindex = 0;
	}

	// Find available songs with both players having their value set to true
	const player1songs = data[player1];
	const player2songs = data[player2];
	let availableSongs = player1songs.filter(value => player2songs.includes(value));
	console.warn(`players ${player1} and ${player2} have in common: <${availableSongs}>`);
	//Object.entries(data).filter(([key, val]) => val[player1] && val[player2]);

	if (availableSongs.length === 0) {
		console.warn(`No common songs found between ${player1} and ${player2}.`);
		process.exit(1); // TODO perhaps just skip this one and go to the next one
	}

	// Select a random song from the available songs
	let currentSong = availableSongs[Math.floor(Math.random() * availableSongs.length)];

	// Remove the song from the data object
	delete data[currentSong]; // will this work?

	// Create a result object
	let result = {
		song: currentSong,
		players: {
			"player1": player1,
			"player2": player2
		}
	};
	
	console.warn("Next: " + JSON.stringify(result));

	return result;
}


function fetchPlayers(dataObj) {
	// Iterate through the entries in the input data object
	// Object.entries(dataObj).forEach((entry) => {
	// 	Object.entries(entry[1]).forEach((value) => {
	// 		const playerName = value[0];
	// 		// Only add unique players to the array
	// 		if (!players.includes(playerName)) {
	// 			players.push(playerName);
	// 		}
	// 	});
	// });
	
	Object.keys(dataObj).forEach((playerName) => {
		// console.warn("Player found: " + playerName);
		players.push(playerName);
	});
	
	console.warn(`Player list: ${players}`)
}

function getAvailablePlayers(){
	return players;
}

function setActivePlayers(pl){
	activePlayers = shufflePlayers(pl);
	console.warn(`Setting players: ${activePlayers}`)
	schedule = PlayerScheduler.optimizeScheduleAdjacent(pl).schedule;
}

function setMusicFolderPath(path){
	musicFolderPath = path;
}

function shufflePlayers(pl){
	for (let i = pl.length - 1; i > 0; i--) {
		// Generate a random index
		const j = Math.floor(Math.random() * (i + 1));
		
		// Swap elements at indices i and j
		[pl[i], pl[j]] = [pl[j], pl[i]];
	}
	return pl;
}
