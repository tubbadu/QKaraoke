#ifndef FILEDOWNLOADER_H
#define FILEDOWNLOADER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

class FileDownloader : public QObject {
	Q_OBJECT
public:
	explicit FileDownloader(QObject *parent = nullptr);
	
	Q_INVOKABLE void downloadFile(const QString &url); // Expose to QML
	
signals:
	void fileDownloaded(QString content);
	void errorOccurred(QString error);
	
private slots:
	void onFileDownloaded(QNetworkReply *reply);
	
private:
	QNetworkAccessManager *manager;
};

#endif // FILEDOWNLOADER_H
