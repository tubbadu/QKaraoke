#include "textfilereader.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>

QByteArray TextFileReader::read(const QString &filename)
{
    QFile file(filename);
    qWarning() << "opening:" << filename;
    
    
    if (!file.open(QIODevice::ReadOnly)) {
        return "error";
    } else {
        return file.readAll();
    }
}
