#include "FileDownloader.h"
#include <QDebug>

FileDownloader::FileDownloader(QObject *parent) : QObject(parent) {
	manager = new QNetworkAccessManager(this);
	connect(manager, &QNetworkAccessManager::finished, this, &FileDownloader::onFileDownloaded);
}

void FileDownloader::downloadFile(const QString &url) {
	QUrl qurl(url);
	QNetworkRequest request(qurl);
	QNetworkReply *reply = manager->get(request);
}


void FileDownloader::onFileDownloaded(QNetworkReply *reply) {
	if (reply->error() == QNetworkReply::NoError) {
		emit fileDownloaded(reply->readAll());
	} else {
		emit errorOccurred(reply->errorString());
	}
	reply->deleteLater();
}
