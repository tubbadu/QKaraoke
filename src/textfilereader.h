#ifndef TEXTFILEREADER_H
#define TEXTFILEREADER_H

#include <QByteArray>
#include <QObject>

class TextFileReader : public QObject
{
    Q_OBJECT

public:
    Q_INVOKABLE QByteArray read(const QString &filename);
};

#endif // TEXTFILEREADER_H
