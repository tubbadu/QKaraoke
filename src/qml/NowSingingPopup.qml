import QtQuick

Rectangle {

    anchors.fill: parent
    function setPlayers(g1, g2){
        text = "Now singing:<br><h1>" + g1 + " & " + g2 + "</h1><br>"
    }
    property alias text: popuptext.text
    color: "black"
    Text {
        id: popuptext
        anchors.centerIn: parent
        width: parent.width * 0.9
        horizontalAlignment: Text.AlignHCenter
        text : "Now singing:<br><h1>tizio & caio</h1><br>"
        textFormat: Text.RichText
        style: Text.Outline
        styleColor: "black"
        color: "white"
        font.pixelSize: parent.width / 40
    }

    visible: true
    z: 100

    Timer {
        id: timer
        interval: 3000
        repeat: false
        running: false
        onTriggered: {
            popup.hide()
        }
    }

    function show(){
        opacityAnimator.stop()
        opacityAnimator.from = 0
        opacityAnimator.to = 1
        opacityAnimator.start()
        timer.start()
    }

    function hide(){
        opacityAnimator.stop()
        opacityAnimator.from = 1
        opacityAnimator.to = 0
        opacityAnimator.start()
    }

    OpacityAnimator {
        id: opacityAnimator
        target: popup;
        from: 0;
        to: 1;
        duration: 1000
        running: false

        onFinished: {
            if(opacityAnimator.from == 0) {
                //player.visible = true
                player.play()
            }
        }
    }
}
