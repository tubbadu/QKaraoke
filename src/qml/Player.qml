import QtQuick
import QtMultimedia

Item{

    anchors.fill: parent
    property alias mediaPlayer: mediaPlayerItem
    property alias source: mediaPlayerItem.source
    property alias playbackState: mediaPlayerItem.playbackState

    property var onTrackEnds: function(source) {
        console.warn(source, "just finished playing!")
        // play next one
        root.nextSong()
    }
    function play(){
        mediaPlayer.play()
    }
    function stop(){
        mediaPlayer.stop()
    }
    function pause(){
        mediaPlayer.pause()
    }
    function setSource(s){
        mediaPlayer.source = s
    }




    MediaPlayer {
        id: mediaPlayerItem
        source: "file:///home/tubbadu/Music/karaoke/Linkin Park - In The End.webm"
        videoOutput: videoOutput
        audioOutput: AudioOutput {}

        onMediaStatusChanged: {
            if(mediaStatus == MediaPlayer.EndOfMedia){
                player.onTrackEnds(source)
            } else if(mediaStatus == MediaPlayer.LoadedMedia) {
                // just started a song
                toolbaritem.slider.to = mediaPlayer.duration
            }
        }

        onPositionChanged: {
            if(toolbaritem.slider.to != mediaPlayer.duration) toolbaritem.slider.to = mediaPlayer.duration
            toolbaritem.slider.value = mediaPlayer.position
        }
    }

    VideoOutput {
        id: videoOutput
        anchors.fill: parent
    }
}
