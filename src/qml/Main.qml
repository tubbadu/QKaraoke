/*
 *
 * TODO:
 *  - rewrite the setup page: the players, instead of being written, are extracted from the Extractor, and can only be toggled on or off.
 *  - The extractor should then be modified to exclude the excluded players
 *  - the weboage should add the possibility for the user to add a name (it should be very easy)
 *
 */


import QtQuick
import QtQuick.Window
import QtQuick.Controls
import QtQuick.Layouts
import QtMultimedia
import Qt.labs.folderlistmodel
import Qt.labs.platform

import TextFileReader
import FileDownloader

// import "https://cdn.jsdelivr.net/gh/nastyox/Rando.js@master/code/plain-javascript/2.0.0/rando-min.js" as Randojs


import "../js/playerScheduler.js" as PlayerScheduler
import "../js/extractor.js" as Extractor

Window {
    id: root
    width: 640
    height: 480
    visible: true
    title: qsTr("QKaraoke")
    visibility: Window.FullScreen

    property var files: []
    property var players: []
    property int np: players.length
    property var matches: []
    property int index: -1

    TextFileReader {
        id: data
        function readData(){
            var d = read("/home/tubbadu/code/qkaraoke/src/data.json") // TODO change this with a relative path
           
            return JSON.parse(d)
        }
    }
    
    FileDownloader {
        id: downloader
        onFileDownloaded: (content) => {
            console.warn("File Content: " + content);
            try {
                const data = JSON.parse(content);
                Extractor.init(data);
                setupView.init();
            }
            catch(err) {
                downloadFailed();
            }
            
        }
        onErrorOccurred: (error) => {
            console.warn("Error: " + error);
            downloadFailed();
        }
        Component.onCompleted: {
            console.warn("Downloading data.json file from 'https://songselector.tubbadu.duckdns.org/download'...");
            downloadFile("https://songselector.tubbadu.duckdns.org/download");
        }
    }

    Shortcut {
        enabled: karaokeView.visible
        sequence: "Space" // Define the key combination to be captured
        onActivated: {
            console.warn("Space key pressed globally!")
            toolbaritem.playpause()
        }
    }

    // function init(){
    //     Extractor.init(data.readData())
    // }
    
    function downloadFailed(){
        Extractor.init(data.readData());
        setupView.init();
        
    }

    function availablePlayers() {
        return Extractor.getAvailablePlayers()
    }
    
    function setActivePlayers(players){
        Extractor.setActivePlayers(players);
    }

    function setSong(songobj){
        player.stop()
        popup.setPlayers(songobj.players.player1, songobj.players.player2)
        player.setSource(setupView.musicFolder.text.trim() + "/" + songobj.song)
        popup.show()
    }

    function nextSong(){
        setSong(Extractor.extractNext())
    }

    function setMusicFolderPath(path){
        Extractor.setMusicFolderPath(path);
    }

    Rectangle {
        id: windowBackground
        color: "black"
        anchors.fill: parent
    }

    Item{
        id: karaokeView
        anchors.fill: parent
        visible: false

        NowSingingPopup {
            id: popup
        }

        Player {
            id: player
        }

        ToolbarItem{
            id: toolbaritem
        }
    }

    SetupView {
        id: setupView
    }

    Item {
        id: closeButtonItem
        height: 35
        width: 35
        anchors.top: parent.top
        anchors.right: parent.right

        Button {
            id: closeButton
            anchors.fill: parent
            icon.name: "window-close"
            visible: closeButtonMousehandler.hovered
            onClicked: {
                Qt.quit()
            }
        }
        HoverHandler {
            id: closeButtonMousehandler
            acceptedDevices: PointerDevice.AllDevices
            cursorShape: Qt.PointingHandCursor
            enabled: true
        }
    }
}
