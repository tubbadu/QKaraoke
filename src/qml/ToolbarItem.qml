import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtMultimedia


Item {
    anchors.bottom: parent.bottom
    anchors.left: parent.left
    anchors.right: parent.right
    property alias slider: mediaslider
    height: 35
    z: 200

    function playpause(){
        playpauseButton.clicked()
    }

    ToolBar{
        anchors.fill: parent
        visible: mousehandler.hovered

        RowLayout{
            anchors.fill: parent
            ToolButton {
                id: playpauseButton
                icon.name: player.playbackState === MediaPlayer.PausedState? "media-playback-start" : "media-playback-pause"
                onClicked: {
                    if(player.playbackState === MediaPlayer.PausedState) {
                        player.play()
                    } else if(player.playbackState === MediaPlayer.PlayingState){
                        player.pause()
                    }
                }
            }

            /*ToolButton { // TODO implement (save in a list the previous songs, and when clicking next first check if the pointer is at the end or not)
                icon.name: "go-previous"
                enabled: index > 0
                onClicked: {
                    root.previousSong()
                }
            }*/
            ToolButton {
                icon.name: "go-next"
                //enabled: index < files.length - 1
                onClicked: {
                    root.nextSong()
                }
            }

            Slider {
                id: mediaslider
                Layout.fillWidth: true
                from: 0
                to: 100

                onMoved: {
                    player.mediaPlayer.position = value
                }
            }
        }
    }

    HoverHandler {
        id: mousehandler
        acceptedDevices: PointerDevice.AllDevices
        cursorShape: Qt.PointingHandCursor
        enabled: true
    }
}
