import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Dialogs as Dialogs
import QtCore
//import "extractor.js" as Extractor


Item{
    anchors.centerIn: parent
    height: parent.height * 0.9
    width: parent.width * 0.7
    visible: true
    property alias musicFolder: musicFolder
    
    function init(){
        // console.warn(">>" + JSON.stringify(playersScrollView.availablePlayersSetting))
        root.availablePlayers().forEach(player => {
            if(playersScrollView.availablePlayersSetting[player] === undefined){
                playersScrollView.availablePlayersSetting[player] = true // append (assuming true) te available players not already saved
            }
        })
        listview.model = Object.keys(playersScrollView.availablePlayersSetting)
    }

    ColumnLayout {
        anchors.fill: parent
        Label {
            text: "Karaoke files location:"
            font.pixelSize: parent.width / 50
        }
        TextField {
            id: musicFolder
            Layout.fillWidth: true
            background: Rectangle{
                color: "#232629"
                border.color: "#5F6265"
                radius: 5
            }
            text: StandardPaths.standardLocations(StandardPaths.MusicLocation)[0]
            font.pixelSize: parent.width / 40

            Dialogs.FolderDialog {
                id: folderDialog
                currentFolder: musicFolder.text
                onAccepted: musicFolder.text = selectedFolder
            }

            ToolButton {
                anchors.right: parent.right
                height: parent.height
                width: height
                onClicked: {
                    folderDialog.open()
                }
                icon.name: "document-open-folder"
            }
        }
        Label {
            text: "Singers list:"
            font.pixelSize: parent.width / 50
        }
        Rectangle{
            color: "#232629"
            border.color: "#5F6265"
            radius: 5
            Layout.fillWidth: true
            Layout.fillHeight: true
            ColumnLayout {
                id: playersScrollView
                anchors.fill: parent

                property var availablePlayersSetting: ({})

                ScrollView {
                    //anchors.fill: parent
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    clip: true
                    Layout.margins: 3

                    ListView {
                        id: listview
                        //model: Object.keys(playersScrollView.availablePlayersSetting)
                        width: parent.width
                        delegate: CheckBox {
                            required property string modelData
                            text: modelData
                            checked: playersScrollView.availablePlayersSetting[modelData]
                            font.pixelSize: playersScrollView.width / 30
                            indicator.width: font.pixelSize * 0.8
                            indicator.height: font.pixelSize * 0.8
                            width: parent.width

                            onClicked: {
                                playersScrollView.availablePlayersSetting[modelData] = checked
                                // console.warn(JSON.stringify(playersScrollView.availablePlayersSetting))
                            }
                        }
                    }
                }

                // Component.onCompleted:  {
                    // root.init()
                    // console.warn(">>" + JSON.stringify(availablePlayersSetting))
                    // root.availablePlayers().forEach(player => {
                    //     if(availablePlayersSetting[player] === undefined){
                    //         availablePlayersSetting[player] = true // append (assuming true) te available players not already saved
                    //     }
                    // })
                    // listview.model = Object.keys(playersScrollView.availablePlayersSetting)
                // }
            }
        }
        Button {
            text: "Start"
            Layout.alignment: Qt.AlignHCenter
            onClicked: {
                var activePlayers = [];
                Object.entries(playersScrollView.availablePlayersSetting).forEach((entry) => {
                    console.warn(entry)
                    if(entry[1]){
                        activePlayers.push(entry[0]);
                    }
                });

                if(activePlayers.length < 2){
                    console.warn("I'm too lazy to handle this error. Minimum 2 singers required!")
                    Qt.quit()
                    return
                }
                root.setActivePlayers(activePlayers);
                setupView.visible = false;
                karaokeView.visible = true;
                root.setMusicFolderPath(musicFolder.text.trim());
                nextSong();
                //Extractor.createMatches()
            }
        }
    }

    Settings {
        id: settings
        property alias karaokeFolder: musicFolder.text
       // property alias karaokePlayers: playersTextArea.text
        // property alias karaokePlayers: playersScrollView.availablePlayersSetting
    }
}
