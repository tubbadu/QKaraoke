#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "textfilereader.h"
#include "FileDownloader.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QCoreApplication::setOrganizationName("KDE");
    QCoreApplication::setOrganizationDomain("kde.org");
    QCoreApplication::setApplicationName("QKaraoke");

    QQmlApplicationEngine engine;
    
    qmlRegisterType<TextFileReader>("TextFileReader", 1, 0, "TextFileReader");
    qmlRegisterType<FileDownloader>("FileDownloader", 1, 0, "FileDownloader");
    
    const QUrl url(u"qrc:/QKaraoke/qml/Main.qml"_qs);
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreationFailed,
        &app, []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
