# Set project name
PROJECT_NAME = qkaraoke

# Help target to display available commands
.PHONY: help
help:
	@echo "Available targets:"
	@echo "  make build   - Configure and compile the project"
	@echo "  make run     - Run the project"
	@echo "  make clean   - Remove the build directory"
	@echo "  make help    - Show this help message"


# Default target
.PHONY: all
all: build run

# Create build directory and run cmake & make
.PHONY: build
build:
	mkdir -p build
	cd build && cmake .. && cmake --build .

# Run the application
.PHONY: run
run:
	./build/src/$(PROJECT_NAME)

# Clean build directory
.PHONY: clean
clean:
	rm -rf build

all: build run
